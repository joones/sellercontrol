<?php 

namespace SellerControl\Repository;

use Doctrine\ORM\EntityRepository;

class CustomerRepository extends EntityRepository
{
    public function findCustomers()
    {
        $entities = $this->findAll();
        $array = [];

        foreach ($entities as $entity) {
            if ($entity->getDeleted() == '0')
                $array[] = $entity;
        }

        return $array;
    }

    public function fetchCustomers()
    {
        $entities = $this->findAll();
        $array = [];

        foreach ($entities as $entity) {
            if ($entity->getDeleted() == '0')
                $array[$entity->getId()] = $entity->getName();
        }

        return $array;
    }

    public function fetchAccountCustomers()
    {
        $entities = $this->findAll();
        $array = [];

        $array[""] = "Selecinar";

        foreach ($entities as $entity) {
            if ($entity->getDeleted() == '0')
                $array[$entity->getId()] = $entity->getName();
        }

        return $array;
    }

    public function customerExists($id) 
    {
        $entity = $this->findOneBy(['id' => $id, 'deleted' => '0']);
        if ($entity) {
            return true;
        } else {
            return false;
        }
    }

    public function emailExists($email) 
    {
        $entity = $this->findOneBy(['email' => $email, 'deleted' => '0']);
        if ($entity) {
            return true;
        } else {
            return false;
        }
    }

    public function emailEditExists($id, $email) 
    {
        $entity = $this->findOneBy(['email' => $email, 'deleted' => '0']);
        if ($entity) {
            if ($entity->getId() != $id) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}