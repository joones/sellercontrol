<?php 

namespace SellerControl\Repository;

use Doctrine\ORM\EntityRepository;

class AppointmentRepository extends EntityRepository
{
    public function findAppointments()
    {
        $entities = $this->findAll();
        $array = [];

        foreach ($entities as $entity) {
            if ($entity->getDeleted() == '0') {
                
                if ($entity->getStatus() == 'O' || $entity->getStatus() == 'C')
                    $array[] = $entity;
            }
        }

        return $array;
    }

    public function fetchAppointments()
    {
        $entities = $this->findAll();
        $array = [];

        $array[""] = "Selecinar";

        foreach ($entities as $entity) {
            if ($entity->getDeleted() == '0')
                $array[$entity->getId()] = $entity->getName();
        }

        return $array;
    }

    public function appointmentExists($id) 
    {
        $entity = $this->findOneBy(['id' => $id, 'deleted' => '0']);
        if ($entity) {
            return true;
        } else {
            return false;
        }
    }

    public function emailExists($email) 
    {
        $entity = $this->findOneBy(['email' => $email, 'deleted' => '0']);
        if ($entity) {
            return true;
        } else {
            return false;
        }
    }

    public function emailEditExists($id, $email) 
    {
        $entity = $this->findOneBy(['email' => $email, 'deleted' => '0']);
        if ($entity) {
            if ($entity->getId() != $id) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}