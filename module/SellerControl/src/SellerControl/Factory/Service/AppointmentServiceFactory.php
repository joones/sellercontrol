<?php

namespace SellerControl\Factory\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use SellerControl\Service\Appointment;

class AppointmentServiceFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {	
   		$em = $controllerManager->get('Doctrine\ORM\EntityManager');

        $service = new Appointment($em, 'SellerControl\Entity\Appointment');
    	
        return $service;
    }
}