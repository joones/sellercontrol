<?php

namespace SellerControl\Factory\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use SellerControl\Service\Customer;

class CustomerServiceFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {	
   		$em = $controllerManager->get('Doctrine\ORM\EntityManager');

        $service = new Customer($em, 'SellerControl\Entity\Customer');
    	
        return $service;
    }
}