<?php

namespace SellerControl\Factory\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use SellerControl\Form\AccountPayableReceivable;

class AccountPayableReceivableFormFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {
    	$em = $controllerManager->get('Doctrine\ORM\EntityManager');

		$customerRepository = $em->getRepository('SellerControl\Entity\Customer');
   		$customers = $customerRepository->fetchAccountCustomers();

    	$userRepository = $em->getRepository('SellerControl\Entity\User');
   		$users = $userRepository->fetchUsers();

        $form = new AccountPayableReceivable(
        	'account', $customers, $users
        );
        
        return $form;
    }
}