<?php

namespace SellerControl\Factory\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use SellerControl\Form\Report;

class ReportFormFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {
    	$modules = [
    		'account-payable' => 'Contas a Pagar',
    		'account-receivable' => 'Contas a Receber',
    		'customer' => 'Clientes',
    		'sale' => 'Vendas'
    	];

        $form = new Report('report', $modules);
        return $form;
    }
}