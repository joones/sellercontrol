<?php

namespace SellerControl\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use SellerControl\Controller\CashierController;

class CashierControllerFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {
    	$serviceManager = $controllerManager->getServiceLocator();

        $em = $serviceManager->get('Doctrine\ORM\EntityManager');

        $controller = new CashierController(
    		$em, 
            'SellerControl\Entity\Cashier'
    	);
    	
        return $controller;
    }
}