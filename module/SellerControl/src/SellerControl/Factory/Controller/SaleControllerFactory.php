<?php

namespace SellerControl\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use SellerControl\Controller\SaleController;

class SaleControllerFactory implements FactoryInterface	
{
	public function createService(ServiceLocatorInterface $controllerManager)
    {
    	$serviceManager = $controllerManager->getServiceLocator();
        $em = $serviceManager->get('Doctrine\ORM\EntityManager');

        $controller = new SaleController(
    		$em, 'SellerControl\Entity\Sale', 'SellerControl\Entity\SaleItem'
    	);

        return $controller;
    }
}