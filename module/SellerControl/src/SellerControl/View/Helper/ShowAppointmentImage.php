<?php

namespace SellerControl\View\Helper;

use Zend\View\Helper\AbstractHelper;

class ShowAppointmentImage extends AbstractHelper {

    public function __invoke($id) {
    	$sm = $this->getView()->getHelperPluginManager()->getServiceLocator();
    	$em = $sm->get('Doctrine\ORM\EntityManager');
    	
    	$appointment = $em->getRepository(
    		'SellerControl\Entity\Appointment'
		)->find($id);

		if (!empty($appointment->getGallery())) {
			$path = "./public_html/uploads/appointment/" . $appointment->getGallery() . '/';
            $op = opendir($path);
      
            while ($file = readdir($op)) {
                $ext = explode(".", $file);
            	if (($ext[1] == "gif") || ($ext[1] == "jpg") || ($ext[1] == "png")) {
            		return "../uploads/appointment/" . $appointment->getGallery() . '/' . $file;
            	} 
            }			
		} else {
			return "../assets/img/semfoto.png";
		}
    }
}
