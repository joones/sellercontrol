<?php

namespace SellerControl\Controller;

use Core\Controller\CrudController;
use Zend\View\Model\ViewModel;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;
use SellerControl\Event\CashierEvent;
use Doctrine\ORM\EntityManager;
use SellerControl\Event\SecurityEvent;

class AccountController extends CrudController
{
    protected $em;
    protected $cEvent;
    protected $securityEvent;

	public function __construct(EntityManager $em, $entity, $form, $service, $controller, $route) {
        $this->em         = $em;
        $this->entity     = $entity;
        $this->form       = $form;
        $this->service    = $service;
        $this->controller = $controller;
        $this->route      = $route;

        $this->securityEvent = new SecurityEvent($this->em);

        $this->cEvent = new CashierEvent($this->em);
    }

    public function payableAction()
    {
        $list = $this->em
                ->getRepository($this->entity)
                ->findBy([
                    'deleted' => '0',
                    'type' => 'P'
                ]);
        
        $page = $this->params()->fromRoute('page');
        
        $paginator = new Paginator(new ArrayAdapter($list));
        $paginator->setCurrentPageNumber($page)
                ->setDefaultItemCountPerPage(12);

        return new ViewModel(array('data' => $paginator, 'page' => $page));
    }

    public function newPayableAction()
    {
        $form = $this->form;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) { 
                $data = $request->getPost()->toArray();
                $data['type'] = 'P';

                if ($data['customer'] != '0') {
                    $customer_exists = $this->em->getRepository(
                        'SellerControl\Entity\Customer'
                    )->customerExists($data['customer']);

                    if (!$customer_exists) {
                        return $this->redirect()->toRoute($this->route, [
                            'controller' => $this->controller,
                            'action' => 'edit-payable',
                            'id' => $data['id']
                        ]);
                    }
                }

                if ($data['user'] != '0') {
                    $user_exists = $this->em->getRepository(
                        'SellerControl\Entity\User'
                    )->userExists($data['user']);

                    if (!$user_exists) {
                        return $this->redirect()->toRoute($this->route, [
                            'controller' => $this->controller,
                            'action' => 'edit-payable',
                            'id' => $data['id']
                        ]);
                    }
                }

                if (
                    !$this->securityEvent->securityVerify($data['document']) || 
                    !$this->securityEvent->securityVerify($data['description']) ||
                    !$this->securityEvent->securityVerify($data['account_value']) ||
                    !$this->securityEvent->securityVerify($data['emission']) ||
                    !$this->securityEvent->securityVerify($data['expiration']) ||
                    !$this->securityEvent->securityVerify($data['notes'])
                ) {
                    return $this->redirect()->toRoute($this->route, [
                        'controller' => 'auth', 'action' => 'logout'
                    ]);
                }

                $this->service->insert($data);

                return $this->redirect()->toRoute($this->route, [
                    'controller' => $this->controller, 'action' => 'payable'
                ]);
            }
        }
        return new ViewModel(['form' => $form]);
    }

    public function editPayableAction()
    {
        $request = $this->getRequest();
        
        $repository = $this->em->getRepository($this->entity);
        $entity = $repository->find($this->params()->fromRoute('id', 0));
        
        if ($this->params()->fromRoute('id', 0)) {
            $this->form->setData($entity->toArray());
        }
        
        if ($request->isPost()) {
            $this->form->setData($request->getPost());
            if ($this->form->isValid()) { 
                $data = $request->getPost()->toArray();
                $data['type'] = 'P';

                if ($data['customer'] != '0') {
                    $customer_exists = $this->em->getRepository(
                        'SellerControl\Entity\Customer'
                    )->customerExists($data['customer']);

                    if (!$customer_exists) {
                        return $this->redirect()->toRoute($this->route, [
                            'controller' => $this->controller,
                            'action' => 'edit-payable',
                            'id' => $data['id']
                        ]);
                    }
                }

                if ($data['user'] != '0') {
                    $user_exists = $this->em->getRepository(
                        'SellerControl\Entity\User'
                    )->userExists($data['user']);

                    if (!$user_exists) {
                        return $this->redirect()->toRoute($this->route, [
                            'controller' => $this->controller,
                            'action' => 'edit-payable',
                            'id' => $data['id']
                        ]);
                    }
                }

                if (
                    !$this->securityEvent->securityVerify($data['document']) || 
                    !$this->securityEvent->securityVerify($data['description']) ||
                    !$this->securityEvent->securityVerify($data['account_value']) ||
                    !$this->securityEvent->securityVerify($data['emission']) ||
                    !$this->securityEvent->securityVerify($data['expiration']) ||
                    !$this->securityEvent->securityVerify($data['notes'])
                ) {
                    return $this->redirect()->toRoute($this->route, [
                        'controller' => 'auth', 'action' => 'logout'
                    ]);
                }

                $this->service->update($data);

                return $this->redirect()->toRoute($this->route, [
                    'controller' => $this->controller, 'action' => 'payable'
                ]);
            }
        }   
        return new ViewModel(['form' => $this->form]);
    }

    public function receivableAction()
    {
        $list = $this->em
                ->getRepository($this->entity)
                ->findBy([
                    'deleted' => '0',
                    'type' => 'R'
                ]);
        
        $page = $this->params()->fromRoute('page');
        
        $paginator = new Paginator(new ArrayAdapter($list));
        $paginator->setCurrentPageNumber($page)
                ->setDefaultItemCountPerPage(12);

        return new ViewModel(array('data' => $paginator, 'page' => $page));
    }

    public function newReceivableAction()
    {
        $form = $this->form;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) { 
                $data = $request->getPost()->toArray();
                $data['type'] = 'R';

                if ($data['customer'] != '0') {
                    $customer_exists = $this->em->getRepository(
                        'SellerControl\Entity\Customer'
                    )->customerExists($data['customer']);

                    if (!$customer_exists) {
                        return $this->redirect()->toRoute($this->route, [
                            'controller' => $this->controller,
                            'action' => 'edit-receivable',
                            'id' => $data['id']
                        ]);
                    }
                }

                if ($data['user'] != '0') {
                    $user_exists = $this->em->getRepository(
                        'SellerControl\Entity\User'
                    )->userExists($data['user']);

                    if (!$user_exists) {
                        return $this->redirect()->toRoute($this->route, [
                            'controller' => $this->controller,
                            'action' => 'edit-receivable',
                            'id' => $data['id']
                        ]);
                    }
                }

                if (
                    !$this->securityEvent->securityVerify($data['document']) || 
                    !$this->securityEvent->securityVerify($data['description']) ||
                    !$this->securityEvent->securityVerify($data['account_value']) ||
                    !$this->securityEvent->securityVerify($data['emission']) ||
                    !$this->securityEvent->securityVerify($data['expiration']) ||
                    !$this->securityEvent->securityVerify($data['notes'])
                ) {
                    return $this->redirect()->toRoute($this->route, [
                        'controller' => 'auth', 'action' => 'logout'
                    ]);
                }

                $this->service->insert($data);

                return $this->redirect()->toRoute($this->route, [
                    'controller' => $this->controller, 'action' => 'receivable'
                ]);
            }
        }
        return new ViewModel(['form' => $form]);
    }

    public function editReceivableAction()
    {
        $request = $this->getRequest();
        
        $repository = $this->em->getRepository($this->entity);
        $entity = $repository->find($this->params()->fromRoute('id', 0));
        
        if ($this->params()->fromRoute('id', 0)) {
            $this->form->setData($entity->toArray());
        }
        
        if ($request->isPost()) {
            $this->form->setData($request->getPost());
            if ($this->form->isValid()) { 
                $data = $request->getPost()->toArray();
                $data['type'] = 'R';

                if ($data['customer'] != '0') {
                    $customer_exists = $this->em->getRepository(
                        'SellerControl\Entity\Customer'
                    )->customerExists($data['customer']);

                    if (!$customer_exists) {
                        return $this->redirect()->toRoute($this->route, [
                            'controller' => $this->controller,
                            'action' => 'edit-receivable',
                            'id' => $data['id']
                        ]);
                    }
                }

                if ($data['user'] != '0') {
                    $user_exists = $this->em->getRepository(
                        'SellerControl\Entity\User'
                    )->userExists($data['user']);

                    if (!$user_exists) {
                        return $this->redirect()->toRoute($this->route, [
                            'controller' => $this->controller,
                            'action' => 'edit-receivable',
                            'id' => $data['id']
                        ]);
                    }
                }

                if (
                    !$this->securityEvent->securityVerify($data['document']) || 
                    !$this->securityEvent->securityVerify($data['description']) ||
                    !$this->securityEvent->securityVerify($data['account_value']) ||
                    !$this->securityEvent->securityVerify($data['emission']) ||
                    !$this->securityEvent->securityVerify($data['expiration']) ||
                    !$this->securityEvent->securityVerify($data['notes'])
                ) {
                    return $this->redirect()->toRoute($this->route, [
                        'controller' => 'auth', 'action' => 'logout'
                    ]);
                }

                $this->service->update($data);

                return $this->redirect()->toRoute($this->route, [
                    'controller' => $this->controller, 'action' => 'receivable'
                ]);
            }
        }   
        return new ViewModel(['form' => $this->form]);
    }

    public function viewPayableAction()
    {
        $id = $this->params()->fromRoute('id', 0);

        $account = $this->em->getRepository($this->entity)
            ->find($id);

        return new ViewModel([
            'data' => $account
        ]);
    }

    public function viewReceivableAction()
    {
        $id = $this->params()->fromRoute('id', 0);

        $account = $this->em->getRepository($this->entity)
            ->find($id);

        return new ViewModel([
            'data' => $account
        ]);
    }

    public function donePayableAction() {
        $id = $this->params()->fromRoute('id', 0);

        $account = $this->em->getRepository($this->entity)
            ->find($id);

        $cashier_amount = $this->cEvent->getCashierDailyExpenses();
        $new_amount = ($cashier_amount + $account->getAccountValue());

        $data = [
            'status' => 'P',
            'id' => $id
        ];

        if ($this->service->update($data)) {
            $this->cEvent->updateDailyExpenses($new_amount);
            return $this->redirect()->toRoute(
                $this->route, ['controller' => $this->controller, 'action' => 'payable']
            );
        }
    }

    public function doneReceivableAction() {
        $id = $this->params()->fromRoute('id', 0);

        $account = $this->em->getRepository($this->entity)
            ->find($id);

        $cashier_amount = $this->cEvent->getCashierDailyReleases();
        $new_amount = ($cashier_amount + $account->getAccountValue());

        $data = [
            'status' => 'P',
            'id' => $id
        ];

        if ($this->service->update($data)) {
            $this->cEvent->updateDailyReleases($new_amount);
            return $this->redirect()->toRoute(
                $this->route, ['controller' => $this->controller, 'action' => 'receivable']
            );
        }
    }

    public function deletePayableAction()
    {
        $id = $this->params()->fromRoute('id', 0);
        return $this->deleteAccountAction($id, 'payable');
    }

    public function deleteReceivableAction()
    {
        $id = $this->params()->fromRoute('id', 0);
        return $this->deleteAccountAction($id, 'receivable');
    }

    public function deleteAccountAction($id, $action) {
        $data = [
            'deleted' => 1,
            'id' => $id
        ];

        if ($this->service->update($data)) {
            return $this->redirect()->toRoute($this->route, [
                'controller' => $this->controller,
                'action' => $action
            ]);
        }
    }
}
