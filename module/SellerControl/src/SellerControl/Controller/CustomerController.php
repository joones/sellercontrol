<?php

namespace SellerControl\Controller;

use Core\Controller\CrudController;
use Zend\View\Model\ViewModel;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;
use Doctrine\ORM\EntityManager;
use SellerControl\Event\SecurityEvent;

class CustomerController extends CrudController
{
    protected $em; 
    protected $securityEvent;

	public function __construct(EntityManager $em, $entity, $form, $service, $controller, $route) {
        $this->em         = $em;
        $this->entity     = $entity;
        $this->form       = $form;
        $this->service    = $service;
        $this->controller = $controller;
        $this->route      = $route;

        $this->securityEvent = new SecurityEvent($this->em);
    }

    public function indexAction() {
        $list = $this->em
            ->getRepository($this->entity)
            ->findCustomers();

        $page = $this->params()->fromRoute('page');

        $paginator = new Paginator(new ArrayAdapter($list));
        $paginator->setCurrentPageNumber($page)
            ->setDefaultItemCountPerPage(12);

        return new ViewModel(array('data' => $paginator, 'page' => $page));
    }

    public function newAction() {
        $form = $this->form;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $request->getPost()->toArray();

                $email_exists = $this->em->getRepository($this->entity)
                    ->emailExists($data['email']);

                if ($email_exists) {
                    return $this->redirect()->toRoute($this->route, [
                        'controller' => $this->controller,
                        'action' => 'new'
                    ]);
                }

                if (
                    !$this->securityEvent->securityVerify($data['name']) ||
                    !$this->securityEvent->securityVerify($data['email']) ||
                    !$this->securityEvent->securityVerify($data['phone']) ||
                    !$this->securityEvent->securityVerify($data['cellphone']) 
                ) {
                    return $this->redirect()->toRoute($this->route, [
                        'controller' => 'auth', 'action' => 'logout'
                    ]);
                }

                $this->service->insert($data);

                return $this->redirect()->toRoute($this->route, [
                    'controller' => $this->controller
                ]);
            }
        }
        return new ViewModel(array('form' => $form));
    }

    public function editAction() {
        $form = $this->form;
        $request = $this->getRequest();
        
        $repository = $this->em->getRepository($this->entity);
        $entity = $repository->find($this->params()->fromRoute('id', 0));
        
        if ($this->params()->fromRoute('id', 0)) {
            $form->setData($entity->toArray());
        }
        
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $request->getPost()->toArray();

                $email_exists = $this->em->getRepository($this->entity)
                    ->emailEditExists($data['email']);

                if ($email_exists) {
                    return $this->redirect()->toRoute($this->route, [
                        'controller' => $this->controller,
                            'action' => 'edit',
                            'id' => $data['id']
                    ]);
                }

                if (
                    !$this->securityEvent->securityVerify($data['name']) ||
                    !$this->securityEvent->securityVerify($data['email']) ||
                    !$this->securityEvent->securityVerify($data['phone']) ||
                    !$this->securityEvent->securityVerify($data['cellphone']) 
                ) {
                    return $this->redirect()->toRoute($this->route, [
                        'controller' => 'auth', 'action' => 'logout'
                    ]);
                }

                $this->service->update($data);

                return $this->redirect()->toRoute($this->route, [
                    'controller' => $this->controller
                ]);
            }
        }
        return new ViewModel(array('form' => $form));
    }

    public function deleteAction() {
        $conn = $this->em->getConnection();
        $query = "UPDATE customer SET deleted=:deleted WHERE id=:id";
        $stmt  = $conn->prepare($query);
        $stmt->bindValue(":deleted", 1);
        $stmt->bindValue(":id", $this->params()->fromRoute('id', 0));

        if ($stmt->execute())
            return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
    }

    public function detailsAction()
    {
        $id = $this->params()->fromRoute('id', 0);
        $customer = $this->em->getRepository($this->entity)->find($id);

        $model = new ViewModel();
        $model->setVariables([
            'customer' => $customer
        ]);

        return $model;
    }
}
