<?php

namespace SellerControl\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use SellerControl\Event\CashierEvent;

class CashierController extends AbstractActionController
{
	protected $em;
	protected $entity;
    protected $cEvent;

	public function __construct(EntityManager $em, $entity)
	{
		$this->em = $em;
		$this->entity = $entity;
        $this->cEvent = new CashierEvent($this->em);
	}

    public function indexAction()
    {
    	$cashier = $this->cEvent->getResume();

        return new ViewModel([
        	'amount' => number_format($cashier['amount'], 2, ',', '.'),
        	'daily_releases' => number_format($cashier['daily_releases'], 2, ',', '.'),
        	'daily_expense' => number_format($cashier['daily_expense'], 2, ',', '.')
        ]);
    }

    public function closeAction()
    {
        if ($this->cEvent->updateDailyAmount()) {
            return $this->redirect()->toRoute($this->route, [
                'controller' => 'cashier'
            ]);
        }
    }
}
