<?php

namespace SellerControl\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use SellerControl\Event\ReportEvent;
use PHPExcel;

class ReportController extends AbstractActionController
{
	protected $em;
	protected $form;
	protected $reportEvent;

	public function __construct(EntityManager $em, $form)
	{
		$this->em = $em;
		$this->form = $form;

		$this->reportEvent = new ReportEvent($this->em);
	}

    public function indexAction()
    {
        return new ViewModel([
        	'form' => $this->form
        ]);
    }

    public function resultAction()
    {
    	$params = $this->params()->fromQuery();

		$result = $this->reportEvent->executeReport($params);

    	$model = new ViewModel();
    	$model->setTerminal(true);
    	$model->setVariables([
    		'module' => $params['module'],
    		'data' => $result
    	]);
    	return $model;
    }

    public function exportAction()
    {
        $params = $this->params()->fromQuery();
        $result = $this->reportEvent->executeReport($params);

        $count = 1;

        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Seller Control")
                                     ->setTitle("Relatório do Sistema");

        switch ($params['module']) {
            case 'account-payable':
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'DOCUMENTO')
                    ->setCellValue('B1', 'DESCRIÇÃO')
                    ->setCellValue('C1', 'VALOR')
                    ->setCellValue('D1', 'EMISSÃO')
                    ->setCellValue('E1', 'VENCIMENTO')
                    ->setCellValue('F1', 'DATA PAGAMENTO')
                    ->setCellValue('G1', 'SITUAÇÃO')
                    ->setCellValue('H1', 'COD. CLIENTE')
                    ->setCellValue('I1', 'COD. USUÁRIO')
                    ->setCellValue('J1', 'OBSERVAÇÕES')
                    ;
                
                foreach ($result as $data) {
                    $count++;

                    $account_value = number_format($data['account_value'], 2, ',', '.');

                    $emission = explode("-", $data['emission'], 3); 
                    $emission_date =  $emission[2] . '/' . $emission[1] . '/' . $emission[0];

                    $expiration = explode("-", $data['expiration'], 3); 
                    $expiration_date =  $expiration[2] . '/' . $expiration[1] . '/' . $expiration[0];

                    if (!empty($data['pay_day'])) {
                        $pay_day = explode("-", $data['pay_day'], 3); 
                        $pay_date =  $pay_day[2] . '/' . $pay_day[1] . '/' . $pay_day[0];

                    } else {
                        $pay_date = null;
                    }

                    switch ($data['status']) {
                        case 'O':
                            $status_label = "Aberta";
                            break;
                        case 'E':
                            $status_label = "Vencida";
                            break;
                        case 'P':
                            $status_label = "Quitada";
                            break;
                        default:
                            $status_label = "Indefinda";
                            break;
                    }

                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $data['document'])
                        ->setCellValue('B' . $count, $data['description'])
                        ->setCellValue('C' . $count, $account_value)
                        ->setCellValue('D' . $count, $emission_date)
                        ->setCellValue('E' . $count, $expiration_date)
                        ->setCellValue('F' . $count, $pay_date)
                        ->setCellValue('G' . $count, $status_label)
                        ->setCellValue('H' . $count, $data['customer_id'])
                        ->setCellValue('I' . $count, $data['user_id'])
                        ->setCellValue('J' . $count,  $data['notes'])
                        ;
                }
                break;
            case 'account-receivable':
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'DOCUMENTO')
                    ->setCellValue('B1', 'DESCRIÇÃO')
                    ->setCellValue('C1', 'VALOR')
                    ->setCellValue('D1', 'EMISSÃO')
                    ->setCellValue('E1', 'VENCIMENTO')
                    ->setCellValue('F1', 'DATA PAGAMENTO')
                    ->setCellValue('G1', 'SITUAÇÃO')
                    ->setCellValue('H1', 'COD. CLIENTE')
                    ->setCellValue('I1', 'COD. USUÁRIO')
                    ->setCellValue('J1', 'OBSERVAÇÕES')
                    ;

                foreach ($result as $data) {
                    $count++;

                    $account_value = number_format($data['account_value'], 2, ',', '.');

                    $emission = explode("-", $data['emission'], 3); 
                    $emission_date =  $emission[2] . '/' . $emission[1] . '/' . $emission[0];

                    $expiration = explode("-", $data['expiration'], 3); 
                    $expiration_date =  $expiration[2] . '/' . $expiration[1] . '/' . $expiration[0];

                    if (!empty($data['pay_day'])) {
                        $pay_day = explode("-", $data['pay_day'], 3); 
                        $pay_date =  $pay_day[2] . '/' . $pay_day[1] . '/' . $pay_day[0];

                    } else {
                        $pay_date = null;
                    }

                    switch ($data['status']) {
                        case 'O':
                            $status_label = "Aberta";
                            break;
                        case 'E':
                            $status_label = "Vencida";
                            break;
                        case 'P':
                            $status_label = "Quitada";
                            break;
                        default:
                            $status_label = "Indefinda";
                            break;
                    }

                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $data['document'])
                        ->setCellValue('B' . $count, $data['description'])
                        ->setCellValue('C' . $count, $account_value)
                        ->setCellValue('D' . $count, $emission_date)
                        ->setCellValue('E' . $count, $expiration_date)
                        ->setCellValue('F' . $count, $pay_date)
                        ->setCellValue('G' . $count, $status_label)
                        ->setCellValue('H' . $count, $data['customer_id'])
                        ->setCellValue('I' . $count, $data['user_id'])
                        ->setCellValue('J' . $count,  $data['notes'])
                        ;
                }
                break;
            case 'customer':
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'NOME')
                    ->setCellValue('B1', 'EMAIL')
                    ->setCellValue('C1', 'TELEFONE')
                    ->setCellValue('D1', 'CELULAR')
                    ->setCellValue('E1', 'DATA CADASTRO')
                    ;

                foreach ($result as $data) {
                    $count++;

                    $created_full = explode(" ", $data['created_at'], 2); 
                    $created = explode("-", $created_full[0], 3); 
                    $created_date =  $created[2] . '/' . $created[1] . '/' . $created[0];

                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $data['name'])
                        ->setCellValue('B' . $count, $data['email'])
                        ->setCellValue('C' . $count, $data['phone'])
                        ->setCellValue('D' . $count, $data['cellphone'])
                        ->setCellValue('E' . $count, $created_date)
                        ;
                }
                break;
            case 'employee':
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'NOME')
                    ->setCellValue('B1', 'SEXO')
                    ->setCellValue('C1', 'DATA NASCIMENTO')
                    ->setCellValue('D1', 'CPF')
                    ->setCellValue('E1', 'RG')
                    ->setCellValue('F1', 'TELEFONE')
                    ->setCellValue('G1', 'CELULAR')
                    ->setCellValue('H1', 'EMAIL')
                    ->setCellValue('I1', 'CARGO')
                    ->setCellValue('J1', 'SALÁRIO')
                    ->setCellValue('K1', 'DATA CONTRATAÇÃO')
                    ->setCellValue('L1', 'DATA SAÍDA')
                    ->setCellValue('M1', 'DIA PAGAMENTO')
                    ->setCellValue('N1', 'ENTRADA')
                    ->setCellValue('O1', 'SAÍDA')
                    ->setCellValue('P1', 'CEP')
                    ->setCellValue('Q1', 'ENDEREÇO')
                    ->setCellValue('R1', 'NÚMERO')
                    ->setCellValue('S1', 'COMPLEMENTO')
                    ->setCellValue('T1', 'BAIRRO')
                    ->setCellValue('U1', 'CIDADE')
                    ->setCellValue('V1', 'UF')
                    ;

                foreach ($result as $data) {
                    $count++;

                    $birthdate = explode("-", $data['birthdate'], 3); 
                    $birthdate_f =  $birthdate[2] . '/' . $birthdate[1] . '/' . $birthdate[0];

                    $hiring = explode("-", $data['hiring_date'], 3); 
                    $hiring_date =  $hiring[2] . '/' . $hiring[1] . '/' . $hiring[0];

                    if (!empty($data['fired_date'])) {
                        $fired = explode("-", $data['fired_date'], 3); 
                        $fired_date =  $fired[2] . '/' . $fired[1] . '/' . $fired[0];
                    } else {
                        $fired_date = "";
                    }

                    $salary = number_format($data['salary'], 2, ',', '.');

                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $data['name'])
                        ->setCellValue('B' . $count, $data['gender'])
                        ->setCellValue('C' . $count, $birthdate_f)
                        ->setCellValue('D' . $count, $data['cpf'])
                        ->setCellValue('E' . $count, $data['rg'])
                        ->setCellValue('F' . $count, $data['phone'])
                        ->setCellValue('G' . $count, $data['cellphone'])
                        ->setCellValue('H' . $count, $data['email'])
                        ->setCellValue('I' . $count, $data['job_role'])
                        ->setCellValue('J' . $count, $salary)
                        ->setCellValue('K' . $count, $hiring_date)
                        ->setCellValue('L' . $count, $fired_date)
                        ->setCellValue('M' . $count, $data['pay_day'])
                        ->setCellValue('N' . $count, $data['entry_time'])
                        ->setCellValue('O' . $count, $data['exit_time'])
                        ->setCellValue('P' . $count, $data['postal_code'])
                        ->setCellValue('Q' . $count, $data['address'])
                        ->setCellValue('R' . $count, $data['number'])
                        ->setCellValue('S' . $count, $data['complement'])
                        ->setCellValue('T' . $count, $data['neighborhood'])
                        ->setCellValue('U' . $count, $data['city'])
                        ->setCellValue('V' . $count, $data['state'])
                        ;
                }
                break;
            case 'inventory':
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'CÓDIGO')
                    ->setCellValue('B1', 'ITEM')
                    ->setCellValue('C1', 'COD. CATEGORIA')
                    ->setCellValue('D1', 'COD. SUBCATEGORIA')
                    ->setCellValue('E1', 'QUANTIDADE')
                    ->setCellValue('F1', 'QUANTIDADE MIN.')
                    ->setCellValue('G1', 'VALOR')
                    ->setCellValue('H1', 'SITUAÇÃO')
                    ->setCellValue('I1', 'DATA CADASTRO')
                    ;

                foreach ($result as $data) {
                    $count++;

                    $created_full = explode(" ", $data['created_at'], 2); 
                    $created = explode("-", $created_full[0], 3); 
                    $created_date =  $created[2] . '/' . $created[1] . '/' . $created[0];

                    $price = number_format($data['price'], 2, ',', '.');

                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $data['code'])
                        ->setCellValue('B' . $count, $data['category_id'])
                        ->setCellValue('C' . $count, $data['subcategory_id'])
                        ->setCellValue('D' . $count, $data['name'])
                        ->setCellValue('E' . $count, $data['qtd'])
                        ->setCellValue('F' . $count, $data['qtd_min'])
                        ->setCellValue('G' . $count, $price)
                        ->setCellValue('H' . $count, $data['status'])
                        ->setCellValue('I' . $count, $created_date)
                        ;
                }
                break;
            case 'category':
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'CÓDIGO')
                    ->setCellValue('B1', 'CATEGORIA')
                    ->setCellValue('C1', 'DATA CADASTRO')
                    ;

                foreach ($result as $data) {
                    $count++;

                    $created_full = explode(" ", $data['created_at'], 2); 
                    $created = explode("-", $created_full[0], 3); 
                    $created_date =  $created[2] . '/' . $created[1] . '/' . $created[0];

                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $data['id'])
                        ->setCellValue('B' . $count, $data['name'])
                        ->setCellValue('C' . $count, $created_date)
                        ;
                }
                break;
            case 'subcategory':
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'CÓDIGO')
                    ->setCellValue('B1', 'COD. CATEGORIA')
                    ->setCellValue('C1', 'SUBCATEGORIA')
                    ->setCellValue('D1', 'DATA CADASTRO')
                    ;

                foreach ($result as $data) {
                    $count++;

                    $created_full = explode(" ", $data['created_at'], 2); 
                    $created = explode("-", $created_full[0], 3); 
                    $created_date =  $created[2] . '/' . $created[1] . '/' . $created[0];

                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $data['id'])
                        ->setCellValue('B' . $count, $data['category_id'])
                        ->setCellValue('C' . $count, $data['name'])
                        ->setCellValue('D' . $count, $created_date)
                        ;
                }
                break;
            case 'order-service':
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'CÓDIGO')
                    ->setCellValue('B1', 'COD. CLIENTE')
                    ->setCellValue('C1', 'DESCRIÇÃO')
                    ->setCellValue('D1', 'DATA')
                    ->setCellValue('E1', 'HORÁRIO')
                    ->setCellValue('F1', 'VALOR')
                    ->setCellValue('G1', 'SITUAÇÃO')
                    ->setCellValue('H1', 'OBSERVAÇÕES')
                    ->setCellValue('I1', 'DATA CADASTRO')
                    ;

                foreach ($result as $data) {
                    $count++;

                    $sv_date = explode("-", $data['service_date'], 3); 
                    $service_date =  $sv_date[2] . '/' . $sv_date[1] . '/' . $sv_date[0];

                    $created = explode("-", $data['created_at'], 3); 
                    $created_date =  $created[2] . '/' . $created[1] . '/' . $created[0];

                    $service_value = number_format($data['service_value'], 2, ',', '.');

                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $data['id'])
                        ->setCellValue('B' . $count, $data['customer_id'])
                        ->setCellValue('C' . $count, $data['description'])
                        ->setCellValue('D' . $count, $service_date)
                        ->setCellValue('E' . $count, $data['service_time'])
                        ->setCellValue('F' . $count, $service_value)
                        ->setCellValue('H' . $count, $data['status'])
                        ->setCellValue('H' . $count, $data['notes'])
                        ->setCellValue('I' . $count, $created_date)
                    ;
                }
                break;
            case 'user':
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'NOME')
                    ->setCellValue('B1', 'COD. PERFIL DE ACESSO')
                    ->setCellValue('C1', 'EMAIL')
                    ->setCellValue('D1', 'SITUAÇÃO')
                    ->setCellValue('E1', 'DATA CADASTRO')
                    ;
                foreach ($result as $data) {
                    $count++;

                    $created = explode("-", $data['created_at'], 3); 
                    $created_date =  $created[2] . '/' . $created[1] . '/' . $created[0];

                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $data['name'])
                        ->setCellValue('B' . $count, $data['role_id'])
                        ->setCellValue('C' . $count, $data['email'])
                        ->setCellValue('H' . $count, $data['status'])
                        ->setCellValue('I' . $count, $created_date)
                    ;
                }
                break;
            case 'sale':
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'CÓDIGO')
                    ->setCellValue('B1', 'DATA DA VENDA')
                    ->setCellValue('C1', 'COD. CLIENTE')
                    ->setCellValue('D1', 'VALOR')
                    ->setCellValue('E1', 'DESCONTO')
                    ->setCellValue('F1', 'TOTAL')
                    ;
                foreach ($result as $data) {
                    $count++;

                    $sale_date_full = explode(" ", $data['sale_date'], 2); 
                    $sale_date = explode("-", $sale_date_full[0], 3); 
                    $sale_time = explode(":", $sale_date_full[1], 3); 
                    $sale_date_time =  $sale_date[2] . '/' . $sale_date[1] . '/' . $sale_date[0] . " às " . $sale_time[0] . ":" . $sale_time[1];

                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $data['id'])
                        ->setCellValue('B' . $count, $sale_date_time)
                        ->setCellValue('C' . $count, $data['customer_id'])
                        ->setCellValue('D' . $count, number_format($data['price'], 2, ",", "."))
                        ->setCellValue('E' . $count, number_format($data['discount'], 2, ",", "."))
                        ->setCellValue('F' . $count, number_format($data['total_price'], 2, ",", "."))
                    ;
                }
                break;
            default:
                break;
        }
        
        $objPHPExcel->getActiveSheet()->setTitle('Relatório do Sistema');
        $objPHPExcel->setActiveSheetIndex(0);

        include PHPEXCEL_ROOT . "PHPExcel/IOFactory.php";
  
        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Relatório_' . time() . '.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');

        die;
        $model = new ViewModel();
        $model->setTerminal(true);
        return $model;
    }
}
