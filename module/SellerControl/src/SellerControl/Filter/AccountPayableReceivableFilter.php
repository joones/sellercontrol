<?php  

namespace SellerControl\Filter;

use Zend\InputFilter\InputFilter;
use Zend\Validator\EmailAddress;

class AccountPayableReceivableFilter extends InputFilter
{
	public function __construct()
	{
		$this->add([
			'name' => 'description',
			'required' => true,
			'filters' => [
				['name' => 'StripTags'],
				['name' => 'StringTrim']
			],
			'validators' => [
				[
					'name' => 'NotEmpty',
					'options' => [
						'messages' => [
							'isEmpty' => 'Preencha o campo DESCRIÇÃO.',
						]
					]
				]
			]
		]);

		$this->add([
			'name' => 'account_value',
			'required' => true,
			'filters' => [
				['name' => 'StripTags'],
				['name' => 'StringTrim']
			],
			'validators' => [
				[
					'name' => 'NotEmpty',
					'options' => [
						'messages' => [
							'isEmpty' => 'Preencha o campo VALOR.',
						]
					]
				]
			]
		]);

		$this->add([
			'name' => 'emission',
			'required' => true,
			'filters' => [
				['name' => 'StripTags'],
				['name' => 'StringTrim']
			],
			'validators' => [
				[
					'name' => 'NotEmpty',
					'options' => [
						'messages' => [
							'isEmpty' => 'Preencha o campo EMISSÃO.',
						]
					]
				]
			]
		]);

		$this->add([
			'name' => 'expiration',
			'required' => true,
			'filters' => [
				['name' => 'StripTags'],
				['name' => 'StringTrim']
			],
			'validators' => [
				[
					'name' => 'NotEmpty',
					'options' => [
						'messages' => [
							'isEmpty' => 'Preencha o campo VENCIMENTO.',
						]
					]
				]
			]
		]);
	}
}