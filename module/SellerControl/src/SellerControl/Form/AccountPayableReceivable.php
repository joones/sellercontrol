<?php

namespace SellerControl\Form;

use Zend\Form\Form;
use Zend\Form\Element\Select;
use SellerControl\Filter\AccountPayableReceivableFilter;

class AccountPayableReceivable extends Form {
    
    public function __construct($name = null, $customers = [], $users = [], $options = []) {
        parent::__construct('account', $options);

        $this->setInputFilter(new AccountPayableReceivableFilter());
        $this->setAttributes(array('method' => 'post', 'role' => 'form', 'class' => 'form-horizontal form-label-left'));
        
        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);
        
        $customer = new Select();
        $customer->setLabel("Atribuir Cliente: ")
                ->setName('customer')
                ->setAttribute('class', 'form-control')
                ->setValueOptions($customers);
        $this->add($customer);

        $user = new Select();
        $user->setLabel("Atribuir Usuário: ")
                ->setName('user')
                ->setAttribute('class', 'form-control')
                ->setValueOptions($users);
        $this->add($user);

        $document = new \Zend\Form\Element\Text('document');
        $document->setLabel("Documento: ")
                ->setAttributes(['class' => 'form-control']);
        $this->add($document);

        $description = new \Zend\Form\Element\Text('description');
        $description->setLabel("Descrição: ")
                ->setAttributes(['class' => 'form-control']);
        $this->add($description);

        $account_value = new \Zend\Form\Element\Text('account_value');
        $account_value->setLabel("Valor: ")
                ->setAttributes([
                'class' => 'form-control',
                'id' => 'frm-account-value'
        ]);
        $this->add($account_value);

        $emission = new \Zend\Form\Element\Date('emission');
        $emission->setLabel("Emissão: ")
                ->setAttributes([
                'class' => 'form-control',
                'id' => 'frm-emission'
        ]);
        $this->add($emission);

        $expiration = new \Zend\Form\Element\Date('expiration');
        $expiration->setLabel("Vencimento: ")
                ->setAttributes([
                'class' => 'form-control',
                'id' => 'frm-expiration'
        ]);
        $this->add($expiration);
        
        $notes = new \Zend\Form\Element\Text('notes');
        $notes->setLabel("Observações: ")
                ->setAttributes(['class' => 'form-control']);
        $this->add($notes);

        $csfr = new \Zend\Form\Element\Csrf('security');
        $this->add($csfr);
        
        $submit = new \Zend\Form\Element\Submit('submit');
        $submit->setLabel('Salvar')
                ->setAttributes(array(
                    'value' => 'Salvar',
                    'class' => 'btn btn-success',
                ));
        $this->add($submit);

        $button = new \Zend\Form\Element\Button('button');
        $button->setLabel('Cancelar')
                ->setAttributes(array(
                    'value' => 'Cancelar',
                    'class' => 'btn btn-default',
                    'id' => 'btn_cancel_request',
                    'style' => 'margin-left: 10px'
                ));
        $this->add($button);
    }
}