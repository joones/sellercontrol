<?php

namespace SellerControl\Form;

use Zend\Form\Form;
use Zend\Form\Element\Select;
use SellerControl\Filter\AppointmentFilter;

class Appointment extends Form {
    
    public function __construct($name = null, $customers = [], $options = []) {
        parent::__construct('appointment', $options);

        $this->setInputFilter(new AppointmentFilter());
        $this->setAttributes(array(
            'method' => 'post', 'role' => 'form', 
            'class' => 'form-horizontal form-label-left',
            'enctype' => "multipart/form-data"
        ));
        
        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);
        
        $customer = new Select();
        $customer->setLabel("Selecione o Cliente: ")
                ->setName('customer')
                ->setAttributes(['class' => 'form-control', 'id' => 'frm-customer'])
                ->setValueOptions($customers);
        $this->add($customer);

        $descripton = new \Zend\Form\Element\Text('description');
        $descripton->setLabel("Descrição: ")
                ->setAttributes(array('class' => 'form-control'));
        $this->add($descripton);

        $price = new \Zend\Form\Element\Text('price');
        $price->setLabel("Valor: ")
                ->setAttributes(array('class' => 'form-control', 'id' => 'frm-price'));
        $this->add($price);

        $time = new \Zend\Form\Element\Text('time');
        $time->setLabel("Data/Hora: ")
                ->setAttributes(array('class' => 'form-control', 'id' => 'frm-time'));
        $this->add($time);

        $gallery = new \Zend\Form\Element\File('gallery');
        $gallery->setLabel("Arquivos: ")
                ->setAttributes(array('multiple' => 'multiple', 'class' => 'form-control'));
        $this->add($gallery);

        $notes = new \Zend\Form\Element\Textarea('notes');
        $notes->setLabel("Observações: ")
                ->setAttributes(array('class' => 'form-control'));
        $this->add($notes);

        $csfr = new \Zend\Form\Element\Csrf('security');
        $this->add($csfr);
        
        $submit = new \Zend\Form\Element\Submit('submit');
        $submit->setLabel('Salvar')
                ->setAttributes(array(
                    'value' => 'Salvar',
                    'class' => 'btn btn-success',
                ));
        $this->add($submit);

        $button = new \Zend\Form\Element\Button('button');
        $button->setLabel('Cancelar')
                ->setAttributes(array(
                    'value' => 'Cancelar',
                    'class' => 'btn btn-default',
                    'id' => 'btn_cancel_request',
                    'style' => 'margin-left: 10px'
                ));
        $this->add($button);
    }
}