<?php 

namespace SellerControl\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="customer")
 * @ORM\Entity(repositoryClass="SellerControl\Repository\CustomerRepository")
 */
class Customer
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	private $id;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	private $email;

	/**
     * @var string
     *
     * @ORM\Column(name="phone", type="string")
     */
    private $phone;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="deleted", type="string")
	 */
	private $deleted;

    /**
     * @var string
     *
     * @ORM\Column(name="cellphone", type="string")
     */
    private $cellphone;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    private $postal_code;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string")
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string")
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="complement", type="string")
     */
    private $complement;

    /**
     * @var string
     *
     * @ORM\Column(name="neighborhood", type="string")
     */
    private $neighborhood;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string")
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string")
     */
    private $state;

	/**
	 * @ORM\Column(type="datetime", name="created_at")
	 * @var string
	 */
	protected $created;

	/**
	 * @ORM\Column(type="datetime", name="updated_at")
	 * @var string
	 */
	protected $updated;

	public function __construct($options = [])
	{
		(new Hydrator\ClassMethods)->hydrate($options, $this);
		$this->created = new \DateTime("now");
		$this->updated = new \DateTime("now");
	}

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	function getEmail() {
        return $this->email;
    }
    
    function setEmail($email) {
        $this->email = $email;
        return $this;
    }

    function getPhone() {
        return $this->phone;
    }
    
    function setPhone($phone) {
        $this->phone = $phone;
        return $this;
    }

    function getCellphone() {
        return $this->cellphone;
    }
    
    function setCellphone($cellphone) {
        $this->cellphone = $cellphone;
        return $this;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postal_code;
    }

    /**
     * @param string $postal_code
     * @return Employee
     */
    public function setPostalCode($postal_code)
    {
        $this->postal_code = $postal_code;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return Employee
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return Employee
     */
    public function setNumber($number)
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return string
     */
    public function getComplement()
    {
        return $this->complement;
    }

    /**
     * @param string $complement
     * @return Employee
     */
    public function setComplement($complement)
    {
        $this->complement = $complement;
        return $this;
    }

    /**
     * @return string
     */
    public function getNeighborhood()
    {
        return $this->neighborhood;
    }

    /**
     * @param string $neighborhood
     * @return Employee
     */
    public function setNeighborhood($neighborhood)
    {
        $this->neighborhood = $neighborhood;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return Employee
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     * @return Employee
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    function getDeleted() {
        return $this->deleted;
    }

    function setDeleted($deleted) {
        $this->deleted = $deleted;
        return $this;
    }

	function getCreated() {
        return $this->created;
    }
    
    function setCreated(\DateTime $created) {
        $this->created = $created;
        return $this;
    }
  
    function getUpdated() {
        return $this->updated;
    }

    function setUpdated(\DateTime $updated) {
        $this->updated = $updated;
        return $this;
    }

    public function toArray() {
        return (new Hydrator\ClassMethods())->extract($this);
    }
}	