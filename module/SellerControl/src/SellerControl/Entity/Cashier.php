<?php 

namespace SellerControl\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="cashier")
 * @ORM\Entity(repositoryClass="SellerControl\Repository\CashierRepository")
 */
class Cashier
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $amount;

	/**
	 * @ORM\Column(type="text", name="daily_releases")
	 * @var string
	 */
	protected $dailyReleases;

	/**
	 * @ORM\Column(type="text", name="daily_expense")
	 * @var string
	 */
	protected $dailyExpense;

	/**
	 * @ORM\Column(type="text", name="daily_amount")
	 * @var string
	 */
	protected $dailyAmount;

	/**
	 * @ORM\OneToOne(targetEntity="SellerControl\Entity\User")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
	 */
	protected $user;

	/**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updated;	

    /**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $closed;

	public function __construct($options = [])
	{
		(new Hydrator\ClassMethods)->hydrate($options, $this);
        $this->updated = new \DateTime("now");
	}

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	public function getAmount()
	{
		return $this->amount;
	}

	public function setAmount($amount)
	{
		$this->amount = $amount;
		return $this;
	}

	public function getDailyReleases()
	{
		return $this->dailyReleases;
	}

	public function setDailyReleases($dailyReleases)
	{
		$this->dailyReleases = $dailyReleases;
		return $this;
	}

	public function getDailyExpense()
	{
		return $this->dailyExpense;
	}

	public function setDailyExpense($dailyExpense)
	{
		$this->dailyExpense = $dailyExpense;
		return $this;
	}

	public function getDailyAmount()
	{
		return $this->dailyAmount;
	}

	public function setDailyAmount($dailyAmount)
	{
		$this->dailyAmount = $dailyAmount;
		return $this;
	}

	public function getUpdated() {
        return $this->updated;
    }

    public function setUpdated(\DateTime $updated) {
        $this->updated = $updated;
        return $this;
    }

	public function getUser()
	{
		return $this->user;
	}

	public function setUser($user)
	{
		$this->user = $user;
		return $this;
	}

	public function getClosed()
	{
		return $this->closed;
	}

	public function setClosed($closed)
	{
		$this->closed = $closed;
		return $this;
	}

	public function toArray() {
        return (new Hydrator\ClassMethods())->extract($this);
    }
}	