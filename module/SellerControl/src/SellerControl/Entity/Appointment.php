<?php 

namespace SellerControl\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="appointment")
 * @ORM\Entity(repositoryClass="SellerControl\Repository\AppointmentRepository")
 */
class Appointment
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;

	/**
	 * @ORM\OneToOne(targetEntity="SellerControl\Entity\Customer")
	 * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
	 */
	protected $customer;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $description;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $price;

	/**
	 * @ORM\Column(type="text", name="appointment_time")
	 * @var string
	 */
	protected $time;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $gallery;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $notes;

	/**
	 * @ORM\Column(type="datetime", name="created_at")
	 * @var string
	 */
	protected $created;

	/**
	 * @ORM\Column(type="datetime", name="updated_at")
	 * @var string
	 */
	protected $updated;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $status;


    /**
     * @var string
     *
     * @ORM\Column(name="deleted", type="string")
     */
    private $deleted;

	public function __construct($options = [])
	{
		(new Hydrator\ClassMethods)->hydrate($options, $this);
		$this->created = new \DateTime("now");
		$this->updated = new \DateTime("now");
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 * @return Appointment
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getCustomer()
	{
		return $this->customer;
	}

	/**
	 * @param string $customer
	 * @return Appointment
	 */
	public function setCustomer($customer)
	{
		$this->customer = $customer;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 * @return Appointment
	 */
	public function setDescription($description)
	{
		$this->description = $description;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getPrice()
	{
		return $this->price;
	}

	/**
	 * @param string $price
	 * @return Appointment
	 */
	public function setPrice($price)
	{
		$this->price = $price;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTime()
	{
		return $this->time;
	}

	/**
	 * @param string $time
	 * @return Appointment
	 */
	public function setTime($time)
	{
		$this->time = $time;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getGallery()
	{
		return $this->gallery;
	}

	/**
	 * @param string $gallery
	 * @return Appointment
	 */
	public function setGallery($gallery)
	{
		$this->gallery = $gallery;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNotes()
	{
		return $this->notes;
	}

	/**
	 * @param string $notes
	 * @return Appointment
	 */
	public function setNotes($notes)
	{
		$this->notes = $notes;
		return $this;
	}

	public function getCreated() {
        return $this->created;
    }
    
    public function setCreated(\DateTime $created) {
        $this->created = $created;
        return $this;
    }
  
    public function getUpdated() {
        return $this->updated;
    }

    public function setUpdated(\DateTime $updated) {
        $this->updated = $updated;
        return $this;
    }

	/**
	 * @return string
	 */
	public function getDeleted()
	{
		return $this->deleted;
	}

	/**
	 * @param string $deleted
	 * @return Appointment
	 */
	public function setDeleted($deleted)
	{
		$this->deleted = $deleted;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param string $status
	 * @return Appointment
	 */
	public function setStatus($status)
	{
		$this->status = $status;
		return $this;
	}

	public function toArray() {
        return (new Hydrator\ClassMethods())->extract($this);
    }
}	