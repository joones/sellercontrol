<?php 

namespace SellerControl\Service;

use Core\Service\AbstractService;
use Doctrine\ORM\EntityManager;
use Zend\Stdlib\Hydrator;

class Appointment extends AbstractService
{
	public function __construct(\Doctrine\ORM\EntityManager $em, $entity)
	{
		parent::__construct($em);
		$this->entity = $entity;
	}

	public function insert(array $data) {
        $entity = new $this->entity($data);

        if ($data['customer'] != "0") {
            $customer = $this->em->getReference("SellerControl\Entity\Customer", $data['customer']);
            $entity->setCustomer($customer);    
        } else {
            $entity->setCustomer(null); 
        }

       if (isset($data['price'])) {
            $price = str_replace(',','.', str_replace('.','', $data['price']));
            $entity->setPrice($price);
        }

        $entity->setStatus('O');
        $entity->setDeleted('0');

        $this->em->persist($entity);
        $this->em->flush();
        return $entity;
    }

    public function update(array $data) {
        $entity = $this->em->getReference($this->entity, $data['id']);
        
        (new Hydrator\ClassMethods())->hydrate($data, $entity);

        if (isset($data['customer']) && $data['customer'] != "0") {
            $customer = $this->em->getReference("SellerControl\Entity\Customer", $data['customer']);
            $entity->setCustomer($customer);    
        } else {
            $entity->setCustomer(null); 
        }

        if (isset($data['price'])) {
            $price = str_replace(',','.', str_replace('.','', $data['price']));
            $entity->setPrice($price);
        }

        $this->em->persist($entity);
        $this->em->flush();
        return $entity;
    }
}