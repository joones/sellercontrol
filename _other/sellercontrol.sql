CREATE DATABASE  IF NOT EXISTS `sellercontrol` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sellercontrol`;
-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: sellercontrol
-- ------------------------------------------------------
-- Server version	5.5.49-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access_log`
--

DROP TABLE IF EXISTS `access_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `attack` text,
  `attack_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access_log`
--

LOCK TABLES `access_log` WRITE;
/*!40000 ALTER TABLE `access_log` DISABLE KEYS */;
INSERT INTO `access_log` VALUES (1,1,'127.0.0.1','&lt;script&gt;alert(2)&lt;/script&gt;','2016-07-26 20:14:36');
/*!40000 ALTER TABLE `access_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_payable_receivable`
--

DROP TABLE IF EXISTS `account_payable_receivable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_payable_receivable` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('P','R') DEFAULT NULL,
  `document` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `account_value` double DEFAULT NULL,
  `emission` date DEFAULT NULL,
  `expiration` date DEFAULT NULL,
  `pay_day` datetime DEFAULT NULL,
  `customer_id` int(11) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `status` enum('O','E','P') DEFAULT NULL,
  `notes` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_payable_receivable`
--

LOCK TABLES `account_payable_receivable` WRITE;
/*!40000 ALTER TABLE `account_payable_receivable` DISABLE KEYS */;
INSERT INTO `account_payable_receivable` VALUES (1,'P','0123','Conta Teste 1',100,'2016-07-01','2016-07-26',NULL,NULL,NULL,'P','','2016-07-25 08:39:48','2016-07-25 08:39:48','0'),(2,'R','0234','Conta teste 2',200,'2016-07-25','2016-07-27',NULL,NULL,NULL,'P','','2016-07-25 08:40:21','2016-07-25 08:40:21','0'),(3,'P','2314123','asdfsdafsad',234.32,'2016-07-25','2016-07-19',NULL,NULL,NULL,'P','','2016-07-25 12:55:31','2016-07-25 12:55:31','0'),(4,'R','','Conta teste 2',450,'2016-07-25','2016-07-28',NULL,NULL,NULL,'P','','2016-07-25 12:56:37','2016-07-25 12:56:37','0'),(5,'P','123423','1234123412',12421,'2016-07-26','2016-07-27',NULL,NULL,NULL,'O','afsdfasdf','2016-07-26 20:01:01','2016-07-26 20:01:01','0');
/*!40000 ALTER TABLE `account_payable_receivable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl`
--

DROP TABLE IF EXISTS `acl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `privilege_id` int(11) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl`
--

LOCK TABLES `acl` WRITE;
/*!40000 ALTER TABLE `acl` DISABLE KEYS */;
INSERT INTO `acl` VALUES (1,1,2,'0'),(2,2,2,'1'),(3,3,2,'0'),(4,4,2,'1'),(5,5,2,'0'),(6,6,2,'1'),(7,7,2,'0'),(8,8,2,'1'),(9,9,2,'0'),(10,10,2,'0'),(11,11,2,'1'),(12,12,2,'0'),(13,13,2,'0'),(14,1,3,'0'),(15,2,3,'0'),(16,3,3,'0'),(17,4,3,'0'),(18,5,3,'0'),(19,6,3,'0'),(20,7,3,'0'),(21,8,3,'0'),(22,9,3,'0'),(23,10,3,'0'),(24,11,3,'0'),(25,12,3,'0'),(26,13,3,'0'),(27,1,4,'0'),(28,2,4,'0'),(29,3,4,'0'),(30,4,4,'0'),(31,5,4,'0'),(32,6,4,'0'),(33,7,4,'0'),(34,8,4,'0'),(35,9,4,'0'),(36,10,4,'0'),(37,11,4,'0'),(38,12,4,'0'),(39,13,4,'0'),(40,15,2,'0'),(41,15,3,'0'),(42,15,4,'0'),(43,16,2,'0'),(44,16,3,'0'),(45,16,4,'0'),(46,17,2,'0'),(47,17,3,'0'),(48,17,4,'0'),(49,1,5,'0'),(50,2,5,'0'),(51,3,5,'0'),(52,4,5,'0'),(53,5,5,'0'),(54,6,5,'0'),(55,7,5,'0'),(56,8,5,'0'),(57,9,5,'0'),(58,10,5,'0'),(59,11,5,'0'),(60,12,5,'0'),(61,13,5,'0'),(62,15,5,'0'),(63,16,5,'0'),(64,17,5,'0');
/*!40000 ALTER TABLE `acl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hash_id` int(11) unsigned NOT NULL,
  `item_id` int(11) unsigned NOT NULL,
  `qtd` int(11) NOT NULL,
  `total` double(10,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `fk_cart_hash_idx` (`hash_id`),
  KEY `fk_cart_inventory_idx` (`item_id`),
  CONSTRAINT `fk_cart_hash` FOREIGN KEY (`hash_id`) REFERENCES `hash` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_cart_inventory` FOREIGN KEY (`item_id`) REFERENCES `inventory` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart_subtotal`
--

DROP TABLE IF EXISTS `cart_subtotal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart_subtotal` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hash_id` int(11) unsigned NOT NULL,
  `customer_id` int(11) unsigned DEFAULT NULL,
  `sale_sum` double(10,2) DEFAULT NULL,
  `discount` double(10,2) DEFAULT NULL,
  `total` double(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cart_subtotal_customer_idx` (`customer_id`),
  KEY `fk_cart_subtotal_hash_idx` (`hash_id`),
  CONSTRAINT `fk_cart_subtotal_customer` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `fk_cart_subtotal_hash` FOREIGN KEY (`hash_id`) REFERENCES `hash` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart_subtotal`
--

LOCK TABLES `cart_subtotal` WRITE;
/*!40000 ALTER TABLE `cart_subtotal` DISABLE KEYS */;
INSERT INTO `cart_subtotal` VALUES (8,4,NULL,NULL,NULL,NULL),(9,4,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `cart_subtotal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cashier`
--

DROP TABLE IF EXISTS `cashier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cashier` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `amount` double DEFAULT NULL,
  `daily_releases` double DEFAULT NULL,
  `daily_expense` double DEFAULT NULL,
  `daily_amount` double DEFAULT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `closed` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cashier`
--

LOCK TABLES `cashier` WRITE;
/*!40000 ALTER TABLE `cashier` DISABLE KEYS */;
INSERT INTO `cashier` VALUES (1,0,450,100,350,1,'2016-07-25 01:00:00','2016-07-25 10:09:53','1'),(9,350,854.43,234.32,620.11,1,'2016-07-25 10:09:53','2016-07-25 13:03:10','1'),(10,970.11,0,0,0,1,'2016-07-25 13:03:11','2016-07-25 13:03:11','0');
/*!40000 ALTER TABLE `cashier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `created_at` date NOT NULL DEFAULT '0000-00-00',
  `updated_at` date NOT NULL DEFAULT '0000-00-00',
  `deleted` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Categoria Teste','2016-07-25','2016-07-25','0'),(2,'Categoria Teste 2','2016-07-25','2016-07-25','0'),(3,' \"SELECT * FROM category                     WHERE id LIKE \'%a%\'                     exec master..xp_cmdshell \'net user test testpass /ADD\'--\"','2016-07-26','2016-07-26','1'),(4,'Categoria Teste d0 JOão QUAL AUSHDAS ','2016-07-26','2016-07-26','0'),(5,'Categoria Teste d0 JOão','2016-07-26','2016-07-26','0'),(6,'Categoria Teste 2','2016-07-26','2016-07-26','0'),(7,'Categoria Teste 2 asdfas','2016-07-26','2016-07-26','0');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `cellphone` varchar(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'Cliente Teste ','cliente@demo.com','(99)9999-9999','(99)99999-9999','2016-07-25 08:36:46','2016-07-25 08:36:46','0'),(2,'sdafasdfsdf','joao.dev93@gmail.com','(21)3412-3412','(41)23412-3412','2016-07-26 16:03:33','2016-07-26 16:03:33','0'),(3,'sdafasdfsdf','joao.dev93@gmail.com','(21)3412-3412','(41)23412-3412','2016-07-26 16:04:21','2016-07-26 16:04:21','0');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `gender` enum('M','F') NOT NULL,
  `birthdate` date NOT NULL,
  `cpf` varchar(15) DEFAULT NULL,
  `rg` varchar(20) DEFAULT NULL,
  `phone` varchar(20) NOT NULL,
  `cellphone` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `job_role` varchar(200) NOT NULL,
  `salary` double NOT NULL,
  `hiring_date` date NOT NULL,
  `fired_date` date DEFAULT NULL,
  `pay_day` char(2) NOT NULL,
  `entry_time` time NOT NULL,
  `exit_time` time NOT NULL,
  `postal_code` varchar(10) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `number` varchar(20) DEFAULT NULL,
  `complement` varchar(200) DEFAULT NULL,
  `neighborhood` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `state` char(2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'João Augusto Bonfante','M','1993-07-10','','','(99)9999-9999','(99)99999-9999','joao.dev93@gmail.com','Programador',3000,'2016-07-01',NULL,'05','08:00:00','12:00:00','','','','','','','','2016-07-25 08:35:46','2016-07-25 08:35:46','0'),(2,'asdfasdfasdf','M','2016-07-26','234.523.4','24352345','(23)4524-3523','(52)34523-4524','joao.deretwertwertv93@gmail.com','wewertwert',223248,'2016-07-27',NULL,'2','22:22:00','22:22:00','23223-233','Rua Correia Galvão, 37','2342','234234','sadfasdf','fasdf','ss','2016-07-26 19:30:42','2016-07-26 19:33:13','0');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hash`
--

DROP TABLE IF EXISTS `hash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hash` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hash_user_idx` (`user_id`),
  CONSTRAINT `fk_hash_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hash`
--

LOCK TABLES `hash` WRITE;
/*!40000 ALTER TABLE `hash` DISABLE KEYS */;
INSERT INTO `hash` VALUES (4,1,'166c4077523d1d034a5fa40f305189f8'),(5,1,'60039f8a08e1db1e33cd060ff4101a50'),(6,1,'610ebd82a4ae370a172f4f223a6b76ec'),(7,1,'7f34ef87ff0a97e439e3371de62af69c'),(8,1,'eaca61eee617c95eedc23975a4cc302a'),(9,1,'d09b9b628c932b87c5d6180992041db2');
/*!40000 ALTER TABLE `hash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory`
--

DROP TABLE IF EXISTS `inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `qtd` int(11) NOT NULL,
  `qtd_min` int(11) NOT NULL,
  `price` double NOT NULL,
  `status` enum('0','1') DEFAULT '1',
  `deleted` enum('0','1') DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `fk_inventory_cat_idx` (`category_id`),
  KEY `fk_inventory_sub_idx` (`subcategory_id`),
  CONSTRAINT `fk_inventory_cat` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `fk_inventory_sub` FOREIGN KEY (`subcategory_id`) REFERENCES `subcategory` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory`
--

LOCK TABLES `inventory` WRITE;
/*!40000 ALTER TABLE `inventory` DISABLE KEYS */;
INSERT INTO `inventory` VALUES (1,1,NULL,'001','Produto Teste',17,5,50,'1','0','2016-07-25 08:33:03','2016-07-25 08:33:03'),(2,2,NULL,'002','Produto Teste 2',49,10,100,'1','0','2016-07-25 08:33:31','2016-07-25 08:33:31'),(3,1,1,'003','Produto Teste 3',37,4,70,'1','0','2016-07-25 08:34:10','2016-07-25 08:34:10'),(4,2,2,'004','Produto Teste 4',99,20,65,'1','0','2016-07-25 08:34:32','2016-07-25 08:34:32'),(5,1,NULL,'007','sadfasdfasdf',234,123,2340,'1','0','2016-07-26 14:35:10','2016-07-26 14:35:10'),(6,1,NULL,'003','Produto Teste 3',37,4,70,'1','0','2016-07-26 14:36:59','2016-07-26 14:36:59');
/*!40000 ALTER TABLE `inventory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `hash` varchar(32) NOT NULL,
  `opened` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (43,NULL,'c6d9b3d1a4895bf20ea3111a0f6b929d',0),(45,NULL,'6267fac80e758ade8f0ef068378d2d88',0);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_item`
--

DROP TABLE IF EXISTS `order_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `item_id` int(11) unsigned NOT NULL,
  `qtd` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_item`
--

LOCK TABLES `order_item` WRITE;
/*!40000 ALTER TABLE `order_item` DISABLE KEYS */;
INSERT INTO `order_item` VALUES (1,44,1,1),(2,44,3,1),(3,44,4,1);
/*!40000 ALTER TABLE `order_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_service`
--

DROP TABLE IF EXISTS `order_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_service` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `service_date` date NOT NULL,
  `service_time` time DEFAULT NULL,
  `service_value` double DEFAULT NULL,
  `notes` text,
  `status` enum('O','C','D') NOT NULL DEFAULT 'O',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` enum('0','1') DEFAULT NULL,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_service`
--

LOCK TABLES `order_service` WRITE;
/*!40000 ALTER TABLE `order_service` DISABLE KEYS */;
INSERT INTO `order_service` VALUES (1,1,'OS Teste','2016-07-26','12:00:00',100,'','D','2016-07-25 08:38:56','2016-07-25 08:39:04','0',1),(2,1,'sdgsdgsfd','2016-07-26','12:00:00',34.43,'sdafasd','D','2016-07-25 12:58:38','2016-07-25 12:59:02','0',1),(3,1,'asdfasdfsd','2016-07-27','22:22:00',2330,'afasdfasdf','O','2016-07-26 18:42:32','2016-07-26 18:46:04','0',1),(4,1,'safsdafasd','2016-07-27','22:22:00',222,'asdfasdf','O','2016-07-26 18:43:56','2016-07-26 18:43:56','0',1);
/*!40000 ALTER TABLE `order_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `privilege`
--

DROP TABLE IF EXISTS `privilege`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `privilege` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned DEFAULT NULL,
  `resource_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_privilege_role_idx` (`role_id`),
  KEY `fk_privilege_resource_idx` (`resource_id`),
  CONSTRAINT `fk_privilege_resource` FOREIGN KEY (`resource_id`) REFERENCES `resource` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_privilege_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `privilege`
--

LOCK TABLES `privilege` WRITE;
/*!40000 ALTER TABLE `privilege` DISABLE KEYS */;
INSERT INTO `privilege` VALUES (1,2,1,'Vendas','2016-07-24 20:37:27','2016-07-24 20:37:27','0'),(2,2,6,'Vendas','2016-07-24 20:37:48','2016-07-24 20:37:48','0'),(3,2,1,'Caixa','2016-07-24 20:38:11','2016-07-24 20:38:11','0'),(4,2,1,'Contas a Pagar','2016-07-24 20:38:20','2016-07-24 20:38:20','0'),(5,2,1,'Contas a Receber','2016-07-24 20:38:24','2016-07-24 20:38:24','0'),(6,2,1,'Clientes','2016-07-24 20:38:34','2016-07-24 20:38:34','0'),(7,2,1,'Funcionários','2016-07-24 20:38:40','2016-07-24 20:38:40','0'),(8,2,1,'Estoque','2016-07-24 20:38:46','2016-07-24 20:38:46','0'),(9,2,1,'Ordem de Serviços','2016-07-24 20:39:00','2016-07-24 20:39:00','0'),(10,2,1,'Relatórios','2016-07-24 20:39:14','2016-07-24 20:39:14','0'),(11,2,1,'Usuários','2016-07-24 20:39:23','2016-07-24 20:39:36','0'),(12,2,1,'Categorias','2016-07-25 08:30:53','2016-07-25 08:30:53','0'),(13,2,1,'Subcategorias','2016-07-25 08:31:02','2016-07-25 08:31:02','0'),(14,3,2,'Contas a Pagar','2016-07-25 12:50:31','2016-07-25 12:50:31','0'),(15,2,1,'asdasdsad','2016-07-26 15:42:25','2016-07-26 15:42:25','0'),(16,2,1,'<script>alert(2)</script>','2016-07-26 15:42:37','2016-07-26 15:42:37','0'),(17,2,6,'Vendas','2016-07-26 15:45:36','2016-07-26 15:45:36','0');
/*!40000 ALTER TABLE `privilege` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource`
--

DROP TABLE IF EXISTS `resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource`
--

LOCK TABLES `resource` WRITE;
/*!40000 ALTER TABLE `resource` DISABLE KEYS */;
INSERT INTO `resource` VALUES (1,'Módulo','2015-11-09 18:43:42','2015-12-15 22:58:27','0'),(2,'Grid','2015-11-09 19:25:55','2015-11-10 20:31:41','0'),(3,'Cadastro','2015-11-10 12:27:19','2015-11-10 12:27:35','0'),(4,'Edição','2015-11-10 12:27:27','2015-11-10 12:27:27','0'),(5,'Exclusão','2015-11-10 12:27:45','2015-11-10 12:27:45','0'),(6,'Visualização','2015-11-10 20:32:37','2015-11-10 20:32:37','0'),(7,'Sincronização','2015-11-10 22:33:08','2015-11-10 22:33:08','0'),(8,'Exportação XLS','2015-11-10 22:33:08','2015-11-10 22:33:08','0');
/*!40000 ALTER TABLE `resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_role_parent_id_idx` (`parent_id`),
  CONSTRAINT `fk_role_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,NULL,'Administrador',1,'2015-09-11 16:00:00','2015-11-10 12:22:34','0'),(2,NULL,'Gerente',0,'2016-07-20 20:04:06','2016-07-23 13:06:01','0'),(3,NULL,'Caixa',0,'2016-07-23 13:06:14','2016-07-23 13:06:14','0'),(4,3,'Balconista',0,'2016-07-23 13:10:30','2016-07-24 15:49:13','0'),(6,NULL,'asdasd',0,'2016-07-26 15:13:08','2016-07-26 15:13:08','0');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sale`
--

DROP TABLE IF EXISTS `sale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `price` double(10,2) DEFAULT '0.00',
  `discount` double(10,2) DEFAULT '0.00',
  `total_price` double(10,2) DEFAULT '0.00',
  `sale_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `hash` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sale_customer_idx` (`customer_id`),
  CONSTRAINT `fk_sale_customer` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sale`
--

LOCK TABLES `sale` WRITE;
/*!40000 ALTER TABLE `sale` DISABLE KEYS */;
INSERT INTO `sale` VALUES (1,1,150.00,NULL,150.00,'2016-07-25 12:36:58','36a4a4ae6881b9b27185f6e632e7f041'),(2,1,375.00,5.00,370.00,'2016-07-25 16:02:30','e4cb1113e1233a1301d2862252cd789d');
/*!40000 ALTER TABLE `sale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sale_item`
--

DROP TABLE IF EXISTS `sale_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sale_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hash` varchar(32) NOT NULL,
  `item_id` int(11) unsigned NOT NULL,
  `item_price` double DEFAULT NULL,
  `qtd` int(11) NOT NULL,
  `total` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sale_item`
--

LOCK TABLES `sale_item` WRITE;
/*!40000 ALTER TABLE `sale_item` DISABLE KEYS */;
INSERT INTO `sale_item` VALUES (14,'36a4a4ae6881b9b27185f6e632e7f041',1,50,1,50),(15,'36a4a4ae6881b9b27185f6e632e7f041',2,100,1,100),(16,'e4cb1113e1233a1301d2862252cd789d',1,50,1,50),(17,'e4cb1113e1233a1301d2862252cd789d',3,70,1,70),(18,'e4cb1113e1233a1301d2862252cd789d',4,65,1,65),(19,'e4cb1113e1233a1301d2862252cd789d',3,70,2,140),(20,'e4cb1113e1233a1301d2862252cd789d',1,50,1,50);
/*!40000 ALTER TABLE `sale_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `service_value` double NOT NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (1,'Teste',123,'asdfasdfsdaf','2016-07-27 09:39:24','2016-07-27 09:39:48','0');
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subcategory`
--

DROP TABLE IF EXISTS `subcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` date NOT NULL DEFAULT '0000-00-00',
  `updated_at` date NOT NULL DEFAULT '0000-00-00',
  `deleted` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_subcategory_category_idx` (`category_id`),
  CONSTRAINT `fk_subcategory_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subcategory`
--

LOCK TABLES `subcategory` WRITE;
/*!40000 ALTER TABLE `subcategory` DISABLE KEYS */;
INSERT INTO `subcategory` VALUES (1,1,'Subcategoria Teste','2016-07-25','2016-07-25','0'),(2,2,'Subcategoria Teste 2','2016-07-25','2016-07-25','0'),(3,1,'<script>alert(2)</script>','2016-07-26','2016-07-26','1'),(4,1,'dsafasdf','2016-07-26','2016-07-26','0'),(5,1,'asdfasddsafasdf','2016-07-26','2016-07-26','0'),(6,2,'adasd','2016-07-26','2016-07-26','0'),(7,1,'asdfadsf','2016-07-26','2016-07-26','0'),(8,2,'asdfasdfas','2016-07-26','2016-07-26','0'),(9,1,'sgdfgsdfgsdfgs','2016-07-26','2016-07-26','0'),(10,1,'trtertrtrtrt','2016-07-26','2016-07-26','0'),(11,4,'JOAJAOAJOSAJODASDAS','2016-07-26','2016-07-26','0'),(12,2,'asdfasdfas','2016-07-26','2016-07-26','0');
/*!40000 ALTER TABLE `subcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_user_1_idx` (`role_id`),
  CONSTRAINT `fk_user_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,1,'João Augusto','joao.dev93@gmail.com','21232f297a57a5a743894a0e4a801fc3','1','2015-11-10 12:00:00','2015-11-13 13:15:45','0'),(2,2,'Demo','demo@demo.com','21232f297a57a5a743894a0e4a801fc3','1','2016-07-20 20:06:01','2016-07-20 20:06:01','1'),(3,2,'Teste','teste@teste.com','21232f297a57a5a743894a0e4a801fc3','1','2016-07-25 12:52:38','2016-07-25 12:52:38','1'),(5,2,'dfasdfasd','caixaasdfasd1@jatecdev.com.br','202cb962ac59075b964b07152d234b70','1','2016-07-26 18:53:06','2016-07-26 18:53:06','1');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-07-28 12:06:31
